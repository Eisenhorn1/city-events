import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import 'bootstrap/scss/bootstrap.scss'
import './styles/app.scss';

ReactDOM.render( < App />, document.getElementById('app'));
