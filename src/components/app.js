import React, {Component} from 'react';
import EventsList from './eventsList';
import SortByButtons from './sortButtons';
import Filter from './filter';
import FavoritesList from "./favoritesList";

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            results: [],
            filtered: [],
            favorites: [],
            storage: [],
            loadingState: true
        };
    }

    componentDidMount() {
        this.getData();
        this.filterList();
    }

    getData = () => {
        fetch('events.json')
        .then(response => response.json())
        .then(json => {
            this.setState({
                results: json.data,
                filtered: json.data,
                loadingState: false
            });
            this.checkStorage();
        })
        .catch(error => {
            console.log(error);
        });
    };

    sortList(prop) {
        const dataObj = this.state.filtered;

        if(prop.prop.term){
            dataObj.sort((a, b) => {
                const priceA = eval(a.price);
                const priceB = eval(b.price);
                return (priceA < priceB) ? -1 : (priceA > priceB) ? 1 : 0;
            });
        }
        else{
            dataObj.sort((a, b) => {
                const priceA = eval(a.price);
                const priceB = eval(b.price);
                return (priceA > priceB) ? -1 : (priceA < priceB) ? 1 : 0;
            });
        }

        this.setState({results: dataObj});
    }

    filterList(prop) {
        const data = this.state.results;
        const filtered = data.filter(item => item.type === prop.prop);

        this.setState({filtered: filtered});
    }

    favorites(prop) {
        const title = document.querySelector('.favorites-title');
        const data = this.state.results;
        let storaged = [];
        let uniqueArray = [];
        let filtered;
        this.state.storage.push(prop.prop);

        if (localStorage.getItem('event') === null) {
            localStorage.setItem('event', JSON.stringify(this.state.storage));
        } else {
            storaged.push(JSON.parse(localStorage.getItem('event')));
            this.state.storage.concat(storaged);
            uniqueArray = [...(new Set(this.state.storage))];
            localStorage.setItem('event', JSON.stringify(uniqueArray));
        }
        filtered = data.filter(item => uniqueArray.includes(item.id));
        this.setState({favorites: filtered});
        title.style.display = 'block';        
    }

    checkStorage() {
        if (localStorage.getItem('event')) {
            const title = document.querySelector('.favorites-title');
            const data = this.state.results;
            const storaged = JSON.parse(localStorage.getItem('event'));
            const filtered = data.filter(item => storaged.includes(item.id));
            this.setState({favorites: filtered});
            title.style.display = 'block';
        }
    }

    render() {
        return (
            <div id="wrap" className="container main-container">
                <div className="btns-wrap">
                    <SortByButtons sortList={prop => this.sortList({prop})} />
                    <Filter filterList = {prop => this.filterList({prop})} data = {this.state.results} />
                </div>
                <div className="events-wrapper">
                    {this.loadingState
                        ? <p>Загрузка</p>
                        : <EventsList favorites={prop => this.favorites({prop})} data={this.state.filtered} />
                    }
                    <FavoritesList data={this.state.favorites}/>
                </div>
            </div>
        );
    }
}
