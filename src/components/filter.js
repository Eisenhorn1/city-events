import React, { Component } from 'react';

class Filter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            types: [
                {name: 'concert', state: true},
                {name: 'exhibition', state: true}
            ]
        }
    }

    onCheckboxChange(type) {
        this.setState({
            types: {
                ...this.state.types,
                [type]: !this.state.types[type]
            }
        });
        // const result = this.props.data.filter(item => item.type === type);

        // result.forEach(element => {
        //     if (element.type === type) {
        //         console.log(element);
        //     }
        // });

        this.props.filterList(type);
    }

    render() {
        return(
            <ul className="filter-list">
                <li>
                    <input
                        type="checkbox"
                        id="typeConcert"
                        value={this.state.types.concert}
                        onChange={this.onCheckboxChange.bind(this, "concert")}
                        defaultChecked={this.state.types[0].state}/>
                    <label htmlFor="typeConcert" className="filter-label">Концерты</label>
                </li>
                <li>
                    <input
                        type="checkbox"
                        id="typeExhibition"
                        value={this.state.types.exhibition}
                        onChange={this.onCheckboxChange.bind(this, "exhibition")}
                        defaultChecked={this.state.types[1].state}/>
                    <label htmlFor="typeExhibition" className="filter-label">Выставки</label>
                </li>
            </ul>
        )
    }
}

export default Filter;
