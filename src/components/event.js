import React, { Component } from 'react';

class Event extends Component{
    constructor(props) {
        super(props);
    }

    addFavorite(id) {
        this.props.favorites(id);
    }

    render() {
        return(
            <div className="event-wrap">
                <div className="event-title">{this.props.title}</div>
                <div className="event-price badge badge-secondary">{this.props.price} ₽</div>
                <button className="btn btn-outline-warning event-favorite"
                        onClick={() => this.addFavorite(this.props.id)}>&#x2605;</button>
                <div className="event-description">{this.props.description}</div>
            </div>
        )
    }
}

export default Event;
