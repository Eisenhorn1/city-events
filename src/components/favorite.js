import React from 'react';

const Favorite = props =>
    <div className="event-wrap">
        <div className="event-title">{props.title}</div>
        <div className="event-price badge badge-secondary">{props.price} ₽</div>
        <div className="event-description">{props.description}</div>
    </div>;

export default Favorite;
