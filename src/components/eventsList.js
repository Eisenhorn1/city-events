import React from 'react';
import Event from './event';

const EventsList = props => {
    const results = props.data;
    const favorite = (prop) => {
        props.favorites(prop.prop);
    };
    let events;

    if (results.length > 0) {
        events = results.map(event =>
            <Event
                id = {event.id}
                title = {event.title}
                price = {event.price}
                description = {event.description}
                type = {event.type}
                favorites = {prop => favorite({prop})}
            />
        );
    }

    return (
        <div className="events">
            <h3>События:</h3>
            {events}
        </div>
    )
};

export default EventsList;
