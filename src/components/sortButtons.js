import React, { Component } from 'react';

class SortByButtons extends Component {
    constructor(props) {
        super(props);
        this.state = {
            priceASC: true
        };
    }

    changeOrder(term) {
        const arrow = document.querySelector('.j-sort-arrow');
        arrow.style.display = 'inline-block';
        arrow.classList.toggle('is-up');

        this.setState({priceASC: term});
        this.props.sortList({term});
    }

    render() {
        return (
            <button className="btn btn-secondary" onClick={()=>this.changeOrder(!this.state.priceASC)}>
                <span className="btn-icon is-up j-sort-arrow">&#x2193;</span>
                <span>Сортировать по цене</span>
            </button>
        )
    }
}

export default SortByButtons;
