import React from 'react';

const FilterInput = props => 
    <li>
        <input 
            type="checkbox" 
            id="typeConcert"
            value={props} 
            // onChange={this.onCheckboxChange.bind(this, "concert")}
            defaultChecked={props}/>
        <label for="typeConcert">{props}</label>
    </li>

export default FilterInput;