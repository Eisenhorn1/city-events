import React from 'react';
import Favorite from './favorite';

const FavoritesList = props => {
    const results = props.data;
    let events;

    if (results.length > 0) {
        events = results.map(event =>
            <Favorite
                id = {event.id}
                title = {event.title}
                price = {event.price}
                description = {event.description}
                type = {event.type}
            />
        );
    }

    return (
        <div className="favorites">
            <h3 className="favorites-title">Избранное:</h3>
            {events}
        </div>
    )
};

export default FavoritesList;
